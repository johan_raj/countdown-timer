"use-strict";

// Dom variables

 let daysCard = document.querySelector('.c-days');
 let hrsCard = document.querySelector('.c-hours');
 let minCard = document.querySelector('.c-minutes');
 let secCard = document.querySelector('.c-seconds');

// Set the date 
const countDownDate = new Date("Nov 24, 2021 19:30:00").getTime();

// Update the count down every 1 second
 let countdown = setInterval(function() {

//   // Get today's date and time
   let currentDate = new Date().getTime();
    

   let calcTime = countDownDate - currentDate;
    
//   // Time calculations for days, hours, minutes and seconds
  let days = Math.floor(calcTime / (1000 * 60 * 60 * 24));
  let hours = Math.floor((calcTime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  let minutes = Math.floor((calcTime % (1000 * 60 * 60)) / (1000 * 60));
  let seconds = Math.floor((calcTime % (1000 * 60)) / 1000);
    


    daysCard.innerHTML =days + "Days";
    hrsCard.textContent = hours + "h";
    minCard.textContent = minutes + "min";
    secCard.textContent = seconds + "sec";
    
  // after countdown in finished
  if (calcTime < 0) {
    document.querySelector('body').style.display='none';
  }
}, 1000);

